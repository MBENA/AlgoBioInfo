rm(list = ls())
source("Algofunction.R")

library(stringr)
library(seqinr)
library(Biostrings)
library(dplyr)


##DATA SAMPLE(Text and Pattern), just give the output of result
PatternCount("GGGAAGTCGGGAAGTGGGCAGGGGAAGTAATAGGGAAGTTATCGGGAAGTGGTTGGGAAGTTCGTGGCAGGGAAGTGGGAAGTGGTGAATGAGCCATACGTTAGGGAAGTCGGGAAGTAAGGGCGGGAAGTTTGGGAAGTTCAACGGAGGGAAGTTGGGAAGTACCGGGAAGTTATGCTGCGGGAAGTATCATGGGAAGTGGGAAGTTGGGAAGTGACGGGAAGTCCTAGGGAAGTAAGTCGGGAAGTCGGGAAGTGGGAAGTAGGGAAGTATAGAGGGAAGTATTTGGGAAGTGTCTTCGGGAAGTGGGGAAGTGGGAAGTCACTTTATTATGGCCTTCGGGAAGTTGGGAAGTGGGGAAGTGGGAAGTTTGGGAAGTGGGGCACGGGAAGTGATGGCACGGGAAGTGGGAAGTGACTTTGGGAAGTTTCGGGTGGGGAAGTGCGGGAAGTGGGAAGTACGTAAATGGGAAGTAGGGAAGTTAGAGGGAAGTGGGGAAGTGGGAAGTGTTCGTTATACAGAAACGGGAAGTGACCGGGAAGTATGGGAAGTGAGGGAAGTGGGAAGTATGGGGAAGTTGCTCAAGGGAAGTCGGGAAGTGGGAAGTTGAATAGGGGAAGTGCCGGGGAAGTGGGAAGTTGGGAAGTGGGGAAGTTCGGGAAGTGGGAAGTTCCCGGGAAGTCTATTCTAGGGAAGTGGGAAGTATGGGAAGTTACGGGAAGTCCGGGGGAAGTGCCGGGAAGTGGGAAGTGGGAAGTATGGGGAAGTTGGGAAGTTAGGGAAGTTGGGAAGTGGGAAGTGCCCGGGAAGTGCGGGAAGTACATCCGCGGGAAGTATGGGGAAGTTGGGGAAGTCGGGGAAGTTGGGGAAGTCGGGAAGTGGGAAGTGGTGGGAAGTCGGAGTGGGAAGTGGGAAGTTTGGGGGAAGTACCGGGAAGTGGGGAAGTGGGAAGTAGGGAAGTCTGGGAAGTAAGGGAAGT
","GGGAAGTGG")

##Frequentword

FrequentWords("ACGTTGCATGTCGCATGATGCATGAGAGCT",4)

####### Find All Occurrences of a Pattern in a String

string_in <- readLines('data/dataset_24021.txt')
pattern <- "CTGCTATCT"
matches <- patternMatch(string_in, pattern, 0)
cat(matches)

# Return a space-separated list of starting positions (in increasing order) 
# where CTTGATCAT appears as a substring in the Vibrio cholerae genome.

string <- readLines('data/Vibrio_cholerae.txt')
pattern2 <- "CTTGATCAT"
matches2 <- patternMatch(string, pattern2, 0)
cat(matches2)

# 
# After solving the Pattern Matching Problem, we discover that ATGATCAAG 
# appears 17 times in the following starting positions of the Vibrio cholerae genome:

string3 <- readLines('data/Vibrio_cholerae.txt')
pattern3 <- "ATGATCAAG"
matches3 <- patternMatch(string3, pattern3, 0)
cat(matches3)

# Results:
#   
#   With the exception of the three occurrences of ATGATCAAG in ori at starting
# positions 151913, 152013, and 152394, no other instances of ATGATCAAG form clumps,
# i.e., appear close to each other in a small region of the genome



genome <- readLines('data/datatestclump.txt')

k <- 5
L <- 50
t <- 4
getClumps(genome, k, L, t)
inputFile <- "data/ClumpFinding3.txt"
inputs <- readLines(con=inputFile, n=2)
genome <- inputs[1]
numbers <- as.integer(strsplit(inputs[2], " ")[[1]])
k <- numbers[1]
L <- numbers[2]
t <- numbers[3]

genome <- "CGGACTCGACAGATGTGAAGAACGACAATGTGAAGACTCGACACGACAGAGTGAAGAGAAGAGGAAACATTGTAA"
k <- 5
L <- 50
t <- 4

clumps <- getClumps(genome, k, L, t)
cat(clumps)

######Skew
# 

data <- "data/skew.txt"
res <- readLines(data, n = 1)
gen <- res[1]

minskew <- skew(gen)
cat(minskew$index)

######Hamming distance

#install.packages("BiocManager")
#BiocManager::install(c("Biostrings"))
neditAt("ACGTACGT", "TACGTACG") #ok good!bioconductor, biostring package

ham1 <- readLines('data/HammingDistance1.txt')
ham2 <- readLines('data/HammingDistance2.txt') 
calcHamming <- neditAt(ham1, ham2)
cat(calcHamming)


#########Approximate Pattern Matching Problem: Find all approximate occurrences of a pattern in a string.

data <- "data/dataset_240221_4.txt"

pattern <- "TGAGAACCAA"
text <- readLines(data)
d <- 5
res <- patternMatchApproximate(text, pattern, d, indexBase=0)
cat(res, file = "data/myfile2.txt")

####Approxpattcount


text <- readLines("data/dataset_240221_6.txt")
pattern <- "AGACAA"
d <- 2

res2 <- approxmatch(text, pattern, d)

# FrequentWordsWithMismatches

##Neighbors

pattn <- "GGCCCAGAG"
bt <- neighbors(pattn, 3)
bt2 <- readLines("data/Neighbors.txt")

bt1 <- neighbors(pattern, 2)
bt2 <- readLines("data/dataset_240229_4.txt")
cat(bt1, file = "data/myfile3.txt")

####Part two:

##First challlenge: MotifEnumeration:



####Median string

difcombin <- function(size){
  allnucleo <- permutations(4, size,c("A","T","G","C"), repeats.allowed=T)
  allcombindna <- apply(allnucleo, 1, paste0, collapse="")
  return(allcombindna)
}


dHamming <- function(pattern, Dna){
            dist <- sapply(dnaCollection, function(dna){
            return(dHamming(pattern, dna))
  })
  
  return(sum(dist))
}

difcombin(2)
medstring <- function(Dna, k){
  dist <- infty
  for (i in kmer) {
  if(dist > d(pattn, Dna)) {
    dist <- d(pattn, Dna)
    med <- pattn
  } 
  return(med)
  }
  }

###Probable k-mer Problem

kmerstring <- function(string){
  if(is.null(string) || is.na(string))
    return(c())
  indata <- strsplit(string, split="")[[1]]
  return(indata)
}


######Greedymotif

# GreedyMotifSearch(Dna, k, t)
# BestMotifs ← motif matrix formed by first k-mers in each string from Dna
# for each k-mer Motif in the first string from Dna
# Motif1 ← Motif
# for i = 2 to t
# form Profile from motifs Motif1, …, Motifi - 1
# Motifi ← Profile-most probable k-mer in the i-th string in Dna
# Motifs ← (Motif1, …, Motift)
# if Score(Motifs) < Score(BestMotifs)
# BestMotifs ← Motifs
# return BestMotifs



